import { combineReducers } from 'redux';
import riskReducer from './risk/reducer'

export default combineReducers({
    riskReducer
})