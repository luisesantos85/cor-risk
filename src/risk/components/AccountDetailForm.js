import React, { Component } from 'react'
import { Panel, Button, FieldSet, TextField, Toolbar, ComboBox, FormPanel, TitleBar} from '@extjs/ext-react';
import PropTypes from 'prop-types';

Ext.require('Ext.MessageBox');

class AccountDetailForm extends Component {

    permissions = {
        C: "Closing Only",
        N: "No Trading",
        Y: "Full Trading"
    };

    permissionStore = new Ext.data.Store({
        autoLoad: false,
        fields: ['name'],
        data:[{name: 'Full Trading', value: 'Y'}, {name: 'Closing Only', value: 'C'}, {name: 'No Trading', value: 'N'}]
    });

    shouldComponentUpdate(nextProps){
        return nextProps.account !== this.props.account;
    }

    componentWillUpdate(props){
        if(props.account){
            this.nameForm.setValues(props.account);
            this.formPermissions.setValues(props.account);
            this.formTop.setValues({
                startingEquity: this.currencyFormat(props.account.startingEquity),
                startingEquityPercent: this.numberFormat(props.account.startingEquityPercent),
                moneyMarket: this.currencyFormat(props.account.moneyMarket),
                startingCashBalance: this.currencyFormat(props.account.startingCashBalance)
            });
            this.formBottom.setValues({
                startingTotalCash: this.currencyFormat(props.account.startingTotalCash),
                startingShortMarketValue: this.currencyFormat(props.account.startingShortMarketValue),
                startingLongMarketValue: this.currencyFormat(props.account.startingLongMarketValue),
                startingTotalMarketValue: this.currencyFormat(props.account.startingTotalMarketValue)
            });
        }
    }

    onChangePermissionClicked(){
        let me = this;
        let permission = me.formPermissions.getValues();

        Ext.Msg.confirm('Confirmation', 'Are you sure you want to change <br /> the permission of account: '+ me.props.account.accountNumber +' to: ' + me.permissions[permission.tradingPermission] + ' ?', function (btn) {
            if (btn === 'yes')me.props.onChangePermissionClicked({accountNumber: me.props.account.accountNumber, tradingPermission: permission.tradingPermission, override: true});
        });
    }

    onClearPermissionClicked(){
        let me = this;

        Ext.Msg.confirm('Confirmation', 'Are you sure you want to clear permissions for account: '+ me.props.account.accountNumber + ' ?', function (btn) {
            if (btn === 'yes')me.props.onClearPermissionClicked({accountNumber: me.props.account.accountNumber});
        });
    }

    currencyFormat(value){
        return Ext.util.Format.currency(value);
    }

    numberFormat(value){
        return Ext.util.Format.number(value, '0.0');
    }

    render() {
        return (
            <Panel  layout="vbox"
                    height="370"
                    scrollable={false}>
                <TitleBar docked="top"
                          style={{zIndex: 2}}>
                    <a>{this.props.title}</a>
                    <Button align="right"
                            iconCls="x-fa fa-envelope"
                            tooltip="Contact Broker"
                            handler={this.props.onShowMessageForm}
                            hidden={this.props.account === null}/>
                </TitleBar>
                <FormPanel  width="100%"
                            layout="vbox"
                            margin="30 10 0 10"
                            ref={(form) => this.nameForm = form}>
                    <TextField label="Name"
                               margin="0 20 5 0"
                               name="accountName"
                               readOnly/>
                    <TextField label="Alert Message"
                               margin="0 20 5 0"
                               name="alertMessage"
                               readOnly/>
                </FormPanel>
                <FieldSet   title="Beginning of Day"
                            layout="hbox"
                            margin="5 15 0 15"
                            scrollable>
                    <FormPanel  layout="vbox"
                                ref={(form) => this.formTop = form}
                                flex="1"
                                header={false}>
                        <TextField label="Equity" name="startingEquity" flex="1" editable={false} readOnly margin="0 10 0 0"/>
                        <TextField label="Equity %" name="startingEquityPercent" flex="1" editable={false} readOnly margin="0 10 0 0"/>
                        <TextField label="Cash" flex="1" name="startingCashBalance" editable={false} readOnly margin="0 10 0 0"/>
                        <TextField label="Total Cash" flex="1" name="startingTotalCash" editable={false} readOnly margin="0 10 0 0"/>
                    </FormPanel>
                    <FormPanel  layout="vbox"
                                flex="1"
                                ref={(form) => this.formBottom = form}
                                header={false}>
                        <TextField label="Money Market" name="moneyMarket" flex="1" editable={false} readOnly/>
                        <TextField label="Short Market" flex="1" name="startingShortMarketValue" editable={false} readOnly/>
                        <TextField label="Long Market" flex="1"  name="startingLongMarketValue" editable={false} readOnly/>
                        <TextField label="Total Market" flex="1" name="startingTotalMarketValue" editable={false} readOnly/>
                    </FormPanel>
                </FieldSet>
                <Toolbar docked="bottom"
                         hidden={this.props.account === null}
                         layout="fit">
                    <FormPanel  layout="hbox"
                                margin="20 10 0 10"
                                flex="1"
                                ref={(form) => this.formPermissions = form}
                                header={false}>
                        <ComboBox flex="1" store={this.permissionStore}
                                  displayField="name"
                                  valueField="value"
                                  margin="0 10 0 0"
                                  name="tradingPermission"/>

                        <Button text="Change Permission" flex="1" handler={this.onChangePermissionClicked.bind(this)}/>
                        <Button text="Clear Permission" flex="1" margin="0 0 0 20" handler={this.onClearPermissionClicked.bind(this)}/>
                    </FormPanel>
                </Toolbar>
            </Panel>
        )
    }
}

AccountDetailForm.propTypes = {
    flex: PropTypes.string,
    title: PropTypes.string,
    account: PropTypes.object,
    onChangePermissionClicked: PropTypes.func,
    onClearPermissionClicked: PropTypes.func,
    onShowMessageForm: PropTypes.func
};

export default AccountDetailForm;