import React, { Component } from 'react'
import { Panel, Grid, Column, TextField, Button, ComboBox, Container, NumberColumn, Menu, TextColumn, MenuItem, Label} from '@extjs/ext-react';
import * as Permissions from './enums/Permissions';
import * as Alerts from './enums/Alerts';
import PropTypes from 'prop-types';
import * as equal from 'deep-equal';

Ext.define('Cor.AlertRowModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.alert-row-model',
    formulas: {
        isAlert: get => {
            if (get('record.alertStatus') === Alerts.RED || get('record.alertStatus') === Alerts.RED_OPEN) {
                return 'corclearing-red-alert';
            }else if(get('record.alertStatus') === Alerts.ORANGE){
                return 'corclearing-orange-alert';
            }else if(get('record.alertStatus') === Alerts.YELLOW){
                return 'corclearing-yellow-alert';
            }else
                return '';
        }
    }
});

class MonitorPanel extends Component {

    accountsRegistry = {};

    alerts = [];

    showOptionStore = new Ext.data.Store({
        autoLoad: false,
        fields: ['name'],
        data:[{name: 'Alerts'}, {name: 'All'}]
    });

    displayAccountsStore = new Ext.data.Store({
        autoLoad: false,
        model: 'Cor.Account'
    });

    constructor() {
        super();

        this.state = {
            show: 'Alerts',
            currentSub: 0,
            record: null,
            isShowingClosePositionsPanel: false,
            isExpanded: false
        }
    }

    shouldComponentUpdate(nextProps, nextState){
        /*Obviously this can be refactor but it is this way for debugging purposes*/
        /*We need to select specifically when to render this component since its properties change all the time*/

        if(nextProps.sub){
            this.accountsRegistry[nextProps.sub.subNo] = nextProps.sub.accounts;
        }

        if(!equal(this.state, nextState)){
            console.log('It will render because the state is different');
            return true;
        }

        if(nextProps.alerts !== this.props.alerts){
            console.log('It will render because alerts arrived');
            return true;
        }

        if(!nextProps.sub){
            return false;
        }

        if(nextProps.sub.subNo === 0){
            console.log('It will render because sub 0 arrived');
            return true;
        }

        if(nextProps.sub.subNo === this.state.currentSub){
            console.log('It will render because sub == to the one used in filter');
            return true;
        }

        return false;

    }

    componentDidUpdate(){
        if(this.props.alerts){
            this.alerts = this.props.alerts.accounts;
        }

        this.loadStore();
    }

    onShowOptionChange(target, value){
        this.displayAccountsStore.removeAll();
        this.setState({show: value});
        this.loadStore();
    }

    onFilterClicked(){
        let value = this.subField.getValue();
        this.setState({currentSub: (!value || value ==='')?0:value});
        this.displayAccountsStore.removeAll();

        if (this.accountsRegistry[this.state.currentSub]) {
            this.displayAccountsStore.loadData(this.accountsRegistry[this.state.currentSub]);
        }
    }

    loadStore(){
        if(this.state.show === 'Alerts'){
            if(this.alerts){
                this.displayAccountsStore.loadRawData(this.alerts);
            }
        }else{
            if(this.state.currentSub === '' || this.state.currentSub === 0){
                if(this.accountsRegistry[0])
                    this.displayAccountsStore.loadRawData(this.accountsRegistry[0]);
            }else {
                if (this.accountsRegistry[this.state.currentSub])
                    this.displayAccountsStore.loadRawData(this.accountsRegistry[this.state.currentSub]);
            }
        }
    }

    toggleMenu = ({ clientX, clientY, event, target }) => {
        const component = Ext.Component.from(target);
        const record = component.row && component.row.getRecord();
        this.setState({ record });

        if (record) {
            this.props.onAccountSelected(record.data)
            this.menu.showAt(clientX, clientY);
        }
    };

    static getPermissionIconClass(record){
        if(record.get('tradingPermissionOverride'))
            return "x-fa fa-lock";
        return (record.get('tradingPermission') === Permissions.FULL_TRADING)?"corclearing-green":(record.get('tradingPermission') === Permissions.CLOSING_ONLY)?"corclearing-yellow":"corclearing-red"
    }

    render() {
        return (
            <Panel  layout="vbox"
                        title="Accounts"
                        tools={[
                                { type: 'right', tooltip: 'Expand', hidden: this.state.isExpanded, handler: () => {this.setState({'isExpanded': true}); this.props.onExpandClicked()}},
                                { type: 'left', tooltip: 'Collapse', hidden: !this.state.isExpanded, handler: () => {this.setState({'isExpanded': false}); this.props.onExpandClicked()}}
                        ]}>
                    <Container margin="2 0 2 0" height="30" layout={{ type: 'hbox', pack: 'right' }}>
                        <ComboBox   label="Show "
                                    labelWidth="50"
                                    width="200"
                                    forceSelection={true}
                                    editable={false}
                                    onChange={this.onShowOptionChange.bind(this)}
                                    value={this.state.show}
                                    displayField="name"
                                    valueField="name"
                                    store={this.showOptionStore}
                                    name="showType"/>
                        <TextField label="Sub Filter"
                                   labelWidth="70"
                                   ref={(field) => this.subField = field}
                                    margin="0 10 0 40"/>
                        <Label     html={'<b>Current Sub Filter: '+ this.state.currentSub + '<b/>'}
                                   hidden={this.state.currentSub === 0 }
                                   margin="5 50 0 50"/>
                        <Button text="Filter"
                                handler={this.onFilterClicked.bind(this)}/>

                    </Container>
                    <Grid ref={(grid) => { this.grid = grid; }}
                          onContextMenu={{
                              element: 'element',
                              fn: this.toggleMenu,
                              preventDefault: true
                          }}
                          itemConfig={{
                              cls: 'row-class',
                              viewModel: {
                                  type: 'alert-row-model',
                              },
                              bind: {
                                  cls: '{isAlert}'
                              }
                          }}
                          store={this.displayAccountsStore}
                          width="100%"
                          flex='1'
                          onSelect={(target, record) => {this.props.onAccountSelected(record.data)}}
                          userSelectable
                          selectable="single">
                        <Menu ref={menu => this.menu = menu}>
                            <MenuItem
                                iconCls="x-fa fa-search"
                                text={'Show Details'}
                                handler={this.props.onShowAccountDetails}
                            />
                        </Menu>
                        <Column width="55"
                                resizable={false}
                                sortable={false}
                                renderer={(value, record) => (
                                <Container layout="hbox" width="100%">
                                    <Button flex="1"
                                            iconCls={MonitorPanel.getPermissionIconClass(record)}
                                            tooltip={record.get('tradingPermission')} />
                                    <Button width="20"
                                            hidden={record.get('alertStatus')!== Alerts.RED_OPEN}
                                            iconCls="corclearing-star"
                                            tooltip="RED OPEN" />
                                </Container>
                                )}/>

                        <Column width="15" dataIndex="alertStatus"
                                renderer={(value, record) => (
                                    <Button width="100%"
                                            hidden={(record.get('alertStatus') !== Alerts.RED)}
                                            iconCls="x-fa fa-file-text-o"
                                            tooltip="Manage Positions"
                                            handler={(target, event) => {this.props.onAccountSelected(record.data); this.props.onShowClosePositions()}}/>
                                )}/>
                        <Column text="Sub" dataIndex="subNo" width="45"/>
                        <Column text="Account" dataIndex="accountNumber"  minWidth="80" flex="1" />
                        <Column text="Type" dataIndex="accountType" width="60"/>
                        <NumberColumn text="Equity" dataIndex="equity" width="95" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Eq. %" dataIndex="equityPercent" width="55" format="000.0" align="right"/>
                        <NumberColumn text="Risk <br /> Ratio" dataIndex="riskRatio" align="right" width="60" format="0.00"/>
                        <NumberColumn text="Max <br /> Shock" dataIndex="maxPositionShockRisk" width="85" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Ruin <br /> Days" dataIndex="ruinDays" width="60" align="right"/>
                        <NumberColumn text="Risk <br /> Adj. Eq." dataIndex="riskEquity" width="100" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Risk <br /> Adj. <br /> Eq. %" dataIndex="riskEquityPercent" width="65" format="000.0" align="right"/>
                        <NumberColumn text="Long <br /> Market" dataIndex="longMarketValue" width="95" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Short <br /> Market" dataIndex="shortMarketValue" width="75" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Total <br /> Market" dataIndex="totalCash" width="85" format="000,000,000.0" align="right"/>
                        <NumberColumn text="Total <br /> Cash" dataIndex="totalMarketValue" width="90" format="000,000,000.0" align="right"/>
                        <TextColumn text="Cnt" dataIndex="activityCount" width="50" format="0" align="right"/>
                    </Grid>
            </Panel>
        )
    }
}

MonitorPanel.propTypes = {
    sub: PropTypes.object,
    alerts: PropTypes.object,
    isLoading: PropTypes.bool,
    onAccountSelected: PropTypes.func,
    onShowAccountDetails: PropTypes.func,
    onShowClosePositions: PropTypes.func,
    onExpandClicked: PropTypes.func
};

export default MonitorPanel;