import React, { Component } from 'react'
import { Panel, Grid, Column, NumberColumn, Menu, MenuItem} from '@extjs/ext-react';
import PropTypes from 'prop-types';

class PositionsPanel extends Component {

    positionStore = new Ext.data.Store({
        autoLoad: false,
        model: 'Cor.Position'
    });

    constructor() {
        super();

        this.state = {
            position: null
        }
    }

    toggleMenu = ({ clientX, clientY, event, target }) => {
        const component = Ext.Component.from(target);
        const position = component.row && component.row.getRecord();
        this.setState({ position });

        if (position) {
            this.menu.showAt(clientX, clientY);
        }
    };

    shouldComponentUpdate(nextProps){
        return this.props.positions !== nextProps.positions;
    }

    componentDidUpdate(){
        this.positionStore.loadRawData(this.props.positions);
    }

    onShowPositionDetails(){
        this.props.onShowPositionDetails(this.state.position.data);
    }

    onShowPositionExecutions(){
        this.props.onShowPositionExecutions({accountNumber: this.state.position.get('accountNumber'), securityId: this.state.position.get('securityId'), currentPage: 1});
    }

    render() {
        return (
            <Panel  layout="vbox"
                    flex={this.props.flex}
                    title={this.props.title}>
                <Grid ref={(grid) => { this.grid = grid; }}
                      store={this.positionStore}
                      width="100%"
                      flex='1'
                      onContextMenu={{
                          element: 'element',
                          fn: this.toggleMenu,
                          preventDefault: true
                      }}
                      userSelectable
                      selectable="single">
                    <Menu ref={menu => this.menu = menu}>
                        <MenuItem
                            iconCls="x-fa fa-search"
                            text={'Show Details'}
                            handler={this.onShowPositionDetails.bind(this)}
                        />
                        <MenuItem
                            iconCls="x-fa fa-align-left"
                            text={'Show Executions'}
                            handler={this.onShowPositionExecutions.bind(this)}
                        />
                    </Menu>
                    <Column text="Sec Id" dataIndex="securityId" width="75"/>
                    <Column text="Type" dataIndex="securityType"  width="50"/>
                    <Column text="Sym" dataIndex="symbol" width="55"/>
                    <NumberColumn text="Shock <br /> Risk" dataIndex="positionShockRisk" width="80" align="right"/>
                    <NumberColumn text="Qty" dataIndex="quantity" width="75" format="000,000,000.0" align="right"/>
                    <NumberColumn text="Up <br /> Rom" dataIndex="upRom" align="right" width="60" format="0.00"/>
                    <NumberColumn text="Down <br /> Rom" dataIndex="downRom" align="right" width="65" format="0.00"/>
                    <NumberColumn text="Market <br /> Value" dataIndex="marketValue" width="80" format="000,000,000.0" align="right"/>
                    <NumberColumn text="Last <br /> Price" dataIndex="lastPrice" width="70" align="right"/>
                    <Column text="Last <br /> Price <br /> Time" dataIndex="lastProceDateTime" width="60"/>
                    <NumberColumn text="Cnt" dataIndex="activityCount" width="50" format="0" align="right"/>
                </Grid>
            </Panel>
        )
    }
}

PositionsPanel.propTypes = {
    flex: PropTypes.string,
    positions: PropTypes.array,
    isLoading: PropTypes.bool,
    title: PropTypes.string,
    onShowPositionDetails: PropTypes.func,
    onShowPositionExecutions: PropTypes.func
};

export default PositionsPanel;