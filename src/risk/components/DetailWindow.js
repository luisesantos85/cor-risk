import React, { Component } from 'react'
import { Grid, Column } from '@extjs/ext-react';
import PropTypes from 'prop-types';

class DetailWindow extends Component {

    propertyStore = new Ext.data.Store({
        autoLoad: false,
        fields: ['name', 'value']
    });

    shouldComponentUpdate(nextProps){
        return nextProps.record !== this.props.record
    }

    componentWillUpdate(props){
        let p = props.record;
        let array = [];
        this.propertyStore.removeAll();
        Object.keys(p).forEach(function(key, idx) {
            array.push({name: key, value: p[key]});
        });
        this.propertyStore.loadRawData(array);
    }

    render() {
        return (
            <Grid ref={(grid) => { this.grid = grid; }}
                  store={this.propertyStore}
                  width="100%"
                  flex='1'
                  userSelectable
                  selectable="single">
                  <Column text="Name" dataIndex="name" width="200"/>
                  <Column text="Value" dataIndex="value" flex="1"/>
            </Grid>
        )
    }
}

DetailWindow.propTypes = {
    width: PropTypes.string,
    record: PropTypes.object,
    height: PropTypes.string,
    flex: PropTypes.string
};

export default DetailWindow;