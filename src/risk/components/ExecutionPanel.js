import React, { Component } from 'react'
import { Grid, Column, Container } from '@extjs/ext-react';
import PagingBar from "./PagingBar";
import PropTypes from 'prop-types';

class ExecutionPanel extends Component {

    executionsStore = new Ext.data.Store({
        autoLoad: false,
        model: 'Cor.Execution'
    });

    paging = {
        pageSize: 200,
        totalPages: 0,
        totalRecords: 0,
        currentPageNumber: 1,
        pagingTitle: ''
    };

    executions = [];

    shouldComponentUpdate(nextProps){
        return this.props.executionObject !== nextProps.executionObject;
    }

    componentWillUpdate(props){
        if(props.executionObject){
            this.executions = props.executionObject.executions;
            let pages = Math.ceil(props.executionObject.totalResultCount / this.paging.pageSize);

            this.paging = {
                ...this.paging,
                totalPages: pages,
                totalRecords: props.executionObject.totalResultCount,
                pagingTitle: 'Showing Page: '+this.paging.currentPageNumber+' of '+pages
            };
        }
    }

    componentDidUpdate(){
        this.executionsStore.loadRawData(this.executions);
    }

    setCurrentPage(page){
        if(page < 1 || page > this.paging.totalPages)
            return;
        this.paging.currentPageNumber = page;
    }

    setPageSize(size){
        this.paging.pageSize = size;
    }

    getExecutions(){
        if(!this.props.position)
            return;

        let nextPage = this.paging.currentPageNumber - 1;

        this.props.getExecutions({
            accountNumber: this.props.position.accountNumber,
            securityId: this.props.position.securityId,
            firstResult:  nextPage * this.paging.pageSize,
            maxResults: this.paging.pageSize,
            sortAscending : true,
            sortColumn: 'transactTime'
        });
    }

    render() {
        return (
            <Container layout="vbox">
                <PagingBar width="100%"
                           height="80"
                           currentPageLabel={this.paging.currentPageLabel}
                           getRecords={this.props.getExecutions}
                           pageSize={this.paging.pageSize}
                           pageNumber={this.paging.currentPageNumber}
                           totalPages={this.paging.totalPages}
                           totalRecords={this.paging.totalRecords}
                           currentPageNumber={this.paging.currentPageNumber}
                           maxItemsPerPage={200}
                           onPageNumberChange={(target, value) => this.setCurrentPage(value)}
                           onPageSizeChange={(target, value) => this.setPageSize(value)}
                           onFirstClicked={(value) => {this.setCurrentPage(value); this.getExecutions()}}
                           onLastClicked={(value) => {this.setCurrentPage(value); this.getExecutions()}}
                           onPreviousClicked={(value) => {this.setCurrentPage(value); this.getExecutions()}}
                           onNextClicked={(value) => {this.setCurrentPage(value); this.getExecutions()}}
                           onReloadClicked={this.getExecutions.bind(this)}
                           showReload={true}
                           layout="vbox"
                           statusTitle={this.paging.pagingTitle}/>
                <Grid ref={(grid) => { this.grid = grid; }}
                      store={this.executionsStore}
                      width="100%"
                      flex='1'
                      userSelectable
                      selectable="single">
                    <Column text="Time" dataIndex="transactTime" width="160"/>
                    <Column text="O/C" dataIndex="openClose" width="50"/>
                    <Column text="Side" dataIndex="side" width="60"/>
                    <Column text="Platform" dataIndex="platform" width="80"/>
                    <Column text="Exec. Qty" dataIndex="lastQty" width="100"/>
                    <Column text="Exec. <br /> Price" dataIndex="lastPx" flex="1"/>
                    <Column text="Order Qty" dataIndex="orderQty" width="80"/>
                    <Column text="Leaves Qty" dataIndex="leavesQty" width="110"/>
                </Grid>
            </Container>
        )
    }
}

ExecutionPanel.propTypes = {
    flex: PropTypes.string,
    executionObject: PropTypes.object,
    position: PropTypes.object,
    getExecutions: PropTypes.func,
    currentPage: PropTypes.number
};

export default ExecutionPanel;