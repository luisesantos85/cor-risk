import React, { Component } from 'react'
import { Container, FieldSet, Button, Label, SpinnerField } from '@extjs/ext-react';
import PropTypes from 'prop-types';

class PagingBar extends Component {

    render() {
        return (
            <Container  layout={this.props.layout}>
                    <Label width="100%"
                           height="10"
                           margin="0 10 0 10"
                           html={this.props.statusTitle}/>
                    <FieldSet layout="hbox">
                        <SpinnerField  labelWidth="110"
                                       label="Page Number"
                                       minValue={1}
                                       value={this.props.pageNumber}
                                       onChange={this.props.onPageNumberChange}
                                       required
                                       margin="0 10 0 0"
                                       flex="1"
                                       editable={false}/>
                        <SpinnerField  labelWidth="120"
                                       label="Items Per Page"
                                       flex="1"
                                       minValue={1}
                                       maxValue={this.props.maxItemsPerPage}
                                       value={this.props.pageSize}
                                       onChange={this.props.onPageSizeChange}
                                       required
                                       editable={false}/>
                        <Button width="30" tooltip="Go First" iconCls="x-fa fa-angle-double-left" handler={() => this.props.onFirstClicked(1)}/>
                        <Button width="30" tooltip="Previous" iconCls="x-fa fa-angle-left" handler={() => this.props.onPreviousClicked(this.props.currentPageNumber - 1)}/>
                        <Button width="30" tooltip="Next" iconCls="x-fa fa-angle-right" handler={() => this.props.onNextClicked(this.props.currentPageNumber + 1)}/>
                        <Button width="30" tooltip="Go Last" iconCls="x-fa fa-angle-double-right" handler={() => this.props.onLastClicked(this.props.totalPages)}/>
                        <Button width="30" tooltip="Reload" iconCls="x-fa fa-undo" handler={() => this.props.onReloadClicked(this.props.totalPages)} hidden={!this.props.showReload}/>
                    </FieldSet>
            </Container>
        )
    }
}

PagingBar.propTypes = {
    currentPageLabel: PropTypes.string,
    flex: PropTypes.string,
    getRecords: PropTypes.func,
    onPageNumberChange: PropTypes.func,
    onPageSizeChange: PropTypes.func,
    pageNumber: PropTypes.number,
    pageSize: PropTypes.number,
    currentPageNumber: PropTypes.number,
    totalPages: PropTypes.number,
    totalRecords: PropTypes.number,
    onNextClicked: PropTypes.func,
    onPreviousClicked: PropTypes.func,
    onFirstClicked: PropTypes.func,
    onLastClicked: PropTypes.func,
    layout: PropTypes.string,
    maxItemsPerPage: PropTypes.number.isRequired,
    onReloadClicked: PropTypes.func,
    showReload: PropTypes.bool.isRequired,
    statusTitle: PropTypes.string.isRequired
};

export default PagingBar;