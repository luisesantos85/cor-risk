import React, { Component } from 'react'
import { Panel, Grid, Column, NumberColumn, Toolbar, Button} from '@extjs/ext-react';
import PropTypes from 'prop-types';

Ext.require([
    'Ext.grid.plugin.Editable',
    'Ext.grid.plugin.CellEditing',
    'Ext.MessageBox'
]);

class ClosePositionsPanel extends Component {

    positionStore = new Ext.data.Store({
        autoLoad: false,
        model: 'Cor.Position'
    });

    constructor() {
        super();

        this.state = {

        }
    }

    shouldComponentUpdate(nextProps){
        return this.props.positions !== nextProps.positions;
    }

    componentDidUpdate(){
        this.positionStore.loadRawData(this.props.positions);
        this.resetPositions();
    }

    resetPositions(){
        this.positionStore.each((position) => {
            position.set('quantityToUpdate', position.get('quantity'));
        });
    }

    onClosePositionClicked(){
        let positions = this.grid.getSelectable().getSelected().items;
        if(positions.length === 0)
            Ext.Msg.alert('Alert', 'No Position has been selected');
        else{
            if(this.isValidQuantities(positions)){
                let message = this.createConfirmationMessage(positions);
                let me = this;
                Ext.Msg.confirm('Submit', 'Are you sure you want to submit the following orders ?<br/><br/>' + message, function(btn){
                    if(btn === 'yes'){
                        let batch = { accountNumber: me.props.accountNumber, orders: []};

                        positions.forEach(function(position) {

                            let order = {
                                bstrAccount:  me.props.accountNumber,
                                bstrSide: me.calculateDirection(position),
                                nQuantity: position.get('quantityToUpdate'),
                                bstrSymbol: position.get('symbol'),
                                bstrTif: 'D',
                                nPriceType: 1,
                                bstrDestination: 'UBS'
                            };

                            batch.orders.push(order);
                        });

                        me.props.onSavePositionsClicked(batch);
                    }
                });
            }else{
                Ext.Msg.alert('Alert', 'Incorrect Position quantities. The quantity you want to sell or buy should not exceed the current position quantity');
            }
        }
    }

    createConfirmationMessage(positions){
        let message = '';
        let me = this;
        positions.forEach(function (position) {
            let direction = (me.calculateDirection(position)==='B')?'Buy':'Sell'
            message += direction + ' ' + Ext.util.Format.number(position.get('quantityToUpdate'), '0,000') + ' ' + position.get('symbol') + '<br/>';
        });
        return message;
    }

    calculateDirection(position){
        return (position.get('quantity') < 0)?'B':'S';
    }

    isValidQuantities(selection){
        let result = true;
        let index = 0;
        selection.forEach(function(position) {
            let qty, qtyToUpdate;
            qty = position.get('quantity');
            qtyToUpdate = position.get('quantityToUpdate');

            if(Math.abs(qty) < qtyToUpdate)
                result = false;

            index++;
        });
        return result;
    }

    render() {
        return (
            <Panel  layout="vbox"
                    header={false}
                    flex={this.props.flex}>
                <Grid ref={(grid) => { this.grid = grid; }}
                      store={this.positionStore}
                      width="100%"
                      flex='1'
                      userSelectable
                      selectable={{
                          checkbox: true,
                          model: 'multi'
                      }}
                      platformConfig={{
                          desktop:{
                              plugins:{
                                  gridcellediting: true
                              }
                          }
                      }}>
                    <Column text="Symbol" dataIndex="symbol" flex="1" />
                    <NumberColumn text="Qty" dataIndex="quantity" flex="1" format="000,000,000.0" align="right"/>
                    <NumberColumn text="Close Qty (Editable)" editable dataIndex="quantityToUpdate" flex="1" format="000,000,000.0" align="right"/>
                    <NumberColumn text="Shock <br /> Risk" dataIndex="positionShockRisk" flex="1" align="right"/>
                    <NumberColumn text="Market <br /> Value" dataIndex="marketValue" flex="1" format="000,000,000.0" align="right"/>
                </Grid>
                <Toolbar docked="bottom" layout={{ type: 'hbox', pack: 'right' }}>
                    <Button text="Reset" handler={this.resetPositions.bind(this)}/>
                    <Button text="Submit" handler={this.onClosePositionClicked.bind(this)} />
                </Toolbar>
            </Panel>
        )
    }
}

ClosePositionsPanel.propTypes = {
    positions: PropTypes.array,
    onSavePositionsClicked: PropTypes.func,
    accountNumber: PropTypes.number
};

export default ClosePositionsPanel;