import React, { Component } from 'react'
import { Panel, Grid, Column, ComboBox, WidgetCell, Button } from '@extjs/ext-react';
import PropTypes from 'prop-types';

Ext.require([
    'Ext.grid.plugin.Editable',
    'Ext.grid.plugin.CellEditing'
]);

class LogGrid extends Component {

    logStore = new Ext.data.Store({
        autoLoad: false,
        model: 'Cor.HistoricAlert'
    });

    constructor() {
        super();
    }

    componentDidMount(){
        this.props.getRecoveries();
    }

    shouldComponentUpdate(nextProps, nextState){
        return this.props.logs !== nextProps.logs;
    }

    componentDidUpdate(){
        this.logStore.loadData(this.props.logs);
    }

    onRefreshClick(){
        this.props.getRecoveries();
    }

    onApplyClicked(record){
        let messages = [];
        record.data.fixExecutionReportMessages.forEach(function(item){
            messages.push(item.messageId);
        });
        this.props.applySolutionToLog({solutionName: record.get('solution'), messageIdentifiers: messages});
    }

    onSolutionChanged(newValue, record){
        record.set('solution', newValue);
    }

    render() {
        return (
            <Panel  layout="vbox"
                    title={this.props.title}
                    tools={[
                        { type: 'refresh', handler: this.onRefreshClick.bind(this), tooltip: 'Reload Logs' }
                    ]}>
                <Grid ref={(grid) => { this.grid = grid; }}
                      store={this.logStore}
                      width="100%"
                      flex='1'
                      userSelectable
                      platformConfig={{
                          desktop: {
                              plugins: {
                                  gridcellediting: true
                              }
                          }
                      }}
                      selectable="single">
                    <Column text="Reason" dataIndex="failureReason" flex="1"/>
                    <Column text="Solution"
                            flex="1"
                            renderer={(value, record) => (
                                <ComboBox valueField="value"
                                          displayField="name"
                                          forceSelection={true}
                                          autoSelect={true}
                                          onChange={(target, newValue) => {this.onSolutionChanged(newValue, record)}}
                                          store={[{name: record.get('solution'), value: record.get('solution')},
                                              {name: 'Ignore', value: 'ignore'},
                                              {name: 'Repeat', value: 'repeat'}]}/>
                            )}
                    />
                    <Column width="100"
                            renderer={(value, record) => (
                                <Button text="Apply" handler={this.onApplyClicked.bind(this, record)}/>
                            )}
                    />
                </Grid>
            </Panel>
        )
    }
}

LogGrid.propTypes = {
    logs: PropTypes.array,
    isLoading: PropTypes.bool,
    getRecoveries: PropTypes.func,
    applySolutionToLog: PropTypes.func
};

export default LogGrid;