import React, { Component } from 'react'
import { FormPanel, TextAreaField, Toolbar, Button } from '@extjs/ext-react';
import PropTypes from 'prop-types';

Ext.require('Ext.MessageBox');

class MessageToBrokerForm extends Component {

    constructor(props){
        super(props);

        this.state = {
            message: ''
        }
    }

    shouldComponentUpdate(nextProps){
        return this.props.account !== nextProps.account
    }

    onSendClicked(){
        let message = this.state.message;
        if(message === '')
            Ext.Msg.alert('Error', 'You can not send empty messages');
        else {
            this.props.onSendMessageToBroker({accountNumber: this.props.account.accountNumber, message: message});
            this.setState({message: ''});
        }
    }

    render() {
        return (
            <FormPanel  layout="fit"
                        header={false}
                        title={this.props.title}>
                <TextAreaField
                    label="Message"
                    name="message"
                    margin="5 10 5 0"
                    labelWidth="70"
                    value={this.state.message}
                    onChange={(target, value) => this.setState({message: value})}
                    maxRows={13}/>
                <Toolbar docked="bottom"
                         layout="fit">
                        <Button text="Send" handler={this.onSendClicked.bind(this)}/>
                </Toolbar>
            </FormPanel>
        )
    }
}

MessageToBrokerForm.propTypes = {
    account: PropTypes.object,
    onSendMessageToBroker: PropTypes.func
};

export default MessageToBrokerForm;