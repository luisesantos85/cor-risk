import React, { Component } from 'react'
import { Container, Button, Menu, MenuItem, TitleBar, TabPanel, Label, Dialog} from '@extjs/ext-react';

import { connect } from 'react-redux';
import { configureDataSource, getPositions, changePermission, sendMessageToBroker, getExecutions, getRecoveries, logOut, applySolutionToLog, closePositions, clearPermission } from '../actions';
import MonitorPanel from "../components/MonitorPanel";
import AccountDetailForm from "../components/AccountDetailForm";
import PositionsPanel from "../components/PositionsPanel";
import MessageToBrokerForm from "../components/MessageToBrokerForm";
import DetailWindow from "../components/DetailWindow";
import ExecutionPanel from "../components/ExecutionPanel";
import LogGrid from "../components/LogGrid";
import ClosePositionsPanel from "../components/ClosePositionsPanel";
import LogInPanel from "./login/LogInPanel";
import '../components/model/model';

/**
 * The main application view
 */
class RiskApplication extends Component {

    constructor(props){
        super(props);

        this.state = {
            accountNo: 0,
            selectedAccount: null,
            detailRecord: null,
            isShowingDetailWindow: false,
            isShowingMessageForm: false,
            isShowingExecutionPanel: false,
            executionPanelTitle: 'Executions',
            detailWindowTitle: '',
            position: null,
            isShowingClosePositionsPanel: false,
            isMonitorExpanded: false
        }
    }

    onLogOut(){
        logOut();
    }

    render() {
        if(this.props.connected) {
            return (
                <Container fullscreen
                           layout="fit">
                    <Container layout="hbox">
                        <TitleBar docked="top"
                                  shadow
                                  margin="0 0 1 0"
                                  height="47"
                                  style={{zIndex: 2}}>
                            <div className="corclearing-logo"/>
                            <a className="corclearing-title">Risk Management Application</a>
                            <a className="corclearing-test-title">{(process.env.NODE_ENV === 'development') ? ' --TEST VERSION--  ' : ''}</a>
                            <Label align="right" html={this.props.connectionStatus} width="600"/>
                            <Button align="right" text="Options">
                                <Menu rel="menu">
                                    <MenuItem text="Log Out" handler={this.onLogOut.bind(this)} iconCls="x-fa fa-user-times"/>
                                </Menu>
                            </Button>
                        </TitleBar>

                        <TabPanel
                            flex={1}
                            shadow
                            onActiveItemChange={() => getRecoveries()}
                            defaults={{
                                cls: "card",
                                tab: {
                                    flex: 0,
                                    minWidth: 100
                                }
                            }}
                            tabBar={{
                                layout: {
                                    pack: 'left'
                                }
                            }}>
                            <Container title="Monitor"
                                       layout="hbox">
                                <Container flex="1"
                                           layout="fit">
                                    <MonitorPanel sub={this.props.sub}
                                                  alerts={this.props.alerts}
                                                  onShowClosePositions={() => this.setState({isShowingClosePositionsPanel: true})}
                                                  onAccountSelected={(account) => {
                                                      this.setState({
                                                          selectedAccount: account,
                                                          accountNo: account.accountNumber
                                                      });
                                                      getPositions(account)
                                                  }}
                                                  onExpandClicked={() => this.setState({isMonitorExpanded: !this.state.isMonitorExpanded})}
                                                  onShowAccountDetails={() => this.setState({
                                                      detailWindowTitle: 'Account Details',
                                                      isShowingDetailWindow: true,
                                                      detailRecord: this.state.selectedAccount
                                                  })}/>
                                </Container>
                                <Container width="730"
                                           layout="vbox"
                                           hidden={this.state.isMonitorExpanded}>
                                    <AccountDetailForm flex="1"
                                                       title={'Account Details: ' + this.state.accountNo}
                                                       account={this.state.selectedAccount}
                                                       onChangePermissionClicked={(permission) => {this.state.selectedAccount.tradingPermission = permission.tradingPermission; changePermission(permission)}}
                                                       onClearPermissionClicked={(account) => {clearPermission(account)}}
                                                       onShowMessageForm={() => {
                                                           this.setState({isShowingMessageForm: true})
                                                       }}/>
                                    <PositionsPanel flex="1"
                                                    positions={this.props.positions}
                                                    title={'Positions for Account: ' + this.state.accountNo}
                                                    onShowPositionDetails={(record) => this.setState({
                                                        detailWindowTitle: 'Position Details',
                                                        isShowingDetailWindow: true,
                                                        detailRecord: record
                                                    })}
                                                    onShowPositionExecutions={(position) => {
                                                        getExecutions({
                                                            accountNumber: position.accountNumber,
                                                            securityId: position.securityId,
                                                            firstResult: 0,
                                                            sortAscending: true,
                                                            sortColumn: 'transactTime'
                                                        });
                                                        this.setState({
                                                            executionPanelTitle: 'Executions',
                                                            isShowingExecutionPanel: true,
                                                            position: position
                                                        })
                                                    }}/>
                                </Container>
                            </Container>
                            <Container title="Log"
                                       layout="fit">
                                <LogGrid getRecoveries={getRecoveries}
                                         logs={this.props.logs}
                                         title="Logs"
                                         applySolutionToLog={applySolutionToLog}/>
                            </Container>
                        </TabPanel>
                        <Dialog
                            displayed={this.state.isShowingMessageForm}
                            title="Send Message To Broker"
                            width="500"
                            height="400"
                            closable
                            closeAction="hide"
                            maskTapHandler={this.onCancel}
                            bodyPadding="20"
                            layout="fit"
                            onHide={() => this.setState({isShowingMessageForm: false})}>
                            <MessageToBrokerForm flex="1"
                                                 account={this.state.selectedAccount}
                                                 onSendMessageToBroker={(message) => {
                                                     this.setState({isShowingMessageForm: false});
                                                     sendMessageToBroker(message)
                                                 }}/>
                        </Dialog>
                        <Dialog
                            displayed={this.state.isShowingDetailWindow}
                            title={this.state.detailWindowTitle}
                            width="700"
                            height="600"
                            resizable
                            closable
                            closeAction="hide"
                            maskTapHandler={this.onCancel}
                            bodyPadding="20"
                            layout="fit"
                            onHide={() => this.setState({isShowingDetailWindow: false})}>
                            <DetailWindow flex="1"
                                          record={this.state.detailRecord}/>
                        </Dialog>
                        <Dialog
                            displayed={this.state.isShowingExecutionPanel}
                            title={this.state.executionPanelTitle}
                            width="800"
                            height="600"
                            resizable
                            closable
                            closeAction="hide"
                            maskTapHandler={this.onCancel}
                            bodyPadding="20"
                            layout="fit"
                            onHide={() => this.setState({isShowingExecutionPanel: false})}>
                            <ExecutionPanel flex="1"
                                            executionObject={this.props.executions}
                                            getExecutions={getExecutions}
                                            position={this.state.position}/>
                        </Dialog>
                        <Dialog
                            displayed={this.state.isShowingClosePositionsPanel}
                            title="Close Positions"
                            width="800"
                            top="20"
                            bottom="20"
                            left="10"
                            resizable
                            draggable={false}
                            closable
                            closeAction="hide"
                            maskTapHandler={this.onCancel}
                            bodyPadding="20"
                            layout="fit"
                            onHide={() => this.setState({isShowingClosePositionsPanel: false})}>
                            <ClosePositionsPanel flex="1"
                                                 positions={this.props.positions}
                                                 onSavePositionsClicked={(batch) => {closePositions(batch); this.setState({isShowingClosePositionsPanel: false});}}
                                                 accountNumber={this.state.accountNo}/>
                        </Dialog>
                    </Container>
                </Container>
            );
        }else{
            return (
                <Container fullscreen
                           layout="fit">
                    <LogInPanel connect={this.props.configureDataSource} />
                </Container>
            );
        }
    }
}

/**
 * This function passes a piece of the store/state to this component in form of a property
 * */
function mapStateToProps(state){
    return {...state.riskReducer}
}

/**
 * This function connects the component to Redux, specifying a function that maps this component s properties to a
 * piece of the state/store and the actions that this component is able to take
 * */
export default connect(mapStateToProps, {
    configureDataSource
})(RiskApplication)