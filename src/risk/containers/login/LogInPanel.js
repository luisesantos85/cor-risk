import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormPanel, TextField, Toolbar, Button, Label, PasswordField } from '@extjs/ext-react';

Ext.require('Ext.MessageBox');

class LogInPanel extends Component {

    constructor(props){
        super(props);

        this.state = {
            message: ''
        }
    }

    render() {
        return (
            <FormPanel  layout="vbox"
                        centered
                        width="600"
                        height="300"
                        ref={(form) => this.form = form}
                        title="Risk Management App Authentication">
                <TextField
                    label="Username"
                    margin="50 100 10 100"
                    name="username"/>
                <PasswordField
                    label="Password"
                    margin="10 100 100 100"
                    name="password"/>
                <Label html={this.state.message}/>
                <Toolbar docked="bottom"
                         layout="fit">
                    <Button text="Send" handler={() => this.props.connect(this.form.getValues())} buttonType="submit" />
                </Toolbar>
            </FormPanel>
        )
    }
}

LogInPanel.propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    flex: PropTypes.string,
    connect: PropTypes.func.isRequired
};

export default LogInPanel;