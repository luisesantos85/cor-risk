import * as Queues from './queues'

const Stomp = require('stompjs');
const url = 'ws://' + window.location.hostname +':61613/stomp' ;
//const url = 'ws://corp.corclearing.com:61613';

export const SET_ACCOUNTS = 'SET_ACCOUNTS';
export const SET_ACTIONS = 'SET_ACTIONS';
export const SET_EXECUTIONS = 'SET_EXECUTIONS';
export const SET_POSITIONS = 'SET_POSITIONS';
export const SET_RISK_ALERTS = 'SET_RISK_ALERTS';
export const SET_RISK_RECOVERY = 'SET_RISK_RECOVERY';
export const SET_CONNECTION_STATUS = 'SET_CONNECTION_STATUS';

let client;
let USERNAME = '';

export function configureDataSource(parameters){
    USERNAME = parameters.username;

    return dispatch => {

        client = Stomp.client(url);
        client.heartbeat.outgoing = 180000;
        client.heartbeat.incoming = 180000;
        client.maxWebSocketFrameSize = 16 * 1024 * 1024;
        client.debug=false;

        client.connect({
            login: USERNAME,
            passcode: parameters.password
        }, function () {
            dispatch(setConnectionStatus('Connected to '+ url, USERNAME, true));
            client.subscribe(Queues.TOPIC_EQUITY_PERCENTAGE, (message) => dispatch(setRankings(JSON.parse(message.body))));
            client.subscribe(Queues.TOPIC_RISK_ALERT, (message) => dispatch(setRiskAlerts(JSON.parse(message.body))));
            client.subscribe(Queues.QUEUE_RISK_RECOVERY_QUERY_RESPONSE + '.' + USERNAME, (message) => {dispatch(setRiskRecovery(JSON.parse(message.body)))});
            client.subscribe(Queues.QUEUE_GET_EXECUTIONS_RESPONSE + '.' + USERNAME, (message) => dispatch(setExecutions(JSON.parse(message.body))));
            client.subscribe(Queues.QUEUE_RISK_RECOVERY_INVOKE_RESPONSE + '.' + USERNAME, (message) => getRecoveries());
            client.subscribe(Queues.QUEUE_GET_POSITIONS_RESPONSE + '.' + USERNAME, (message) => {dispatch(setPositions(JSON.parse(message.body)))});
        }, function(error){
            dispatch(setConnectionStatus(error, '', false));
        });
    }
}

export function logOut(){
    client.disconnect(() => location.reload());
}

export function setConnectionStatus(status, user, isConnected){
    return {
        type: SET_CONNECTION_STATUS,
        status,
        user,
        isConnected
    }
}

export function setRankings(sub){
    return {
        type: SET_ACCOUNTS,
        sub
    }
}

export function setRiskAlerts(alerts){
    return {
        type: SET_RISK_ALERTS,
        alerts
    }
}

export function setRiskRecovery(recoveries){
    return {
        type: SET_RISK_RECOVERY,
        recoveries
    }
}

export function setExecutions(executions){
    return {
        type: SET_EXECUTIONS,
        executions
    }
}

export function setActions(actions){
    return {
        type: SET_ACTIONS,
        actions
    }
}

export function setPositions(positions){
    return {
        type: SET_POSITIONS,
        positions
    }
}

export function getPositions(account){
    client.send(Queues.QUEUE_GET_POSITIONS, {'reply-to': Queues.QUEUE_GET_POSITIONS_RESPONSE + '.' + USERNAME, 'content-length': false}, JSON.stringify(String(account.accountNumber)));
}

export function changePermission(permission){
    client.send(Queues.QUEUE_UPDATE_TRADING_PERMISSION, {'content-length': false}, JSON.stringify(permission));
}

export function clearPermission(account){
    client.send(Queues.QUEUE_CLEAR_TRADING_PERMISSION, {'content-length': false}, JSON.stringify(account));
}

export function sendMessageToBroker(message){
    client.send(Queues.QUEUE_RISK_PLATFORM_MESSAGE, {'content-length': false}, JSON.stringify(message));
}

export function getExecutions(position){
    client.send(Queues.QUEUE_GET_EXECUTIONS, {'reply-to': Queues.QUEUE_GET_EXECUTIONS_RESPONSE + '.' + USERNAME, 'content-length': false}, JSON.stringify(position));
}

export function getRecoveries(){
    client.send(Queues.QUEUE_RISK_RECOVERY_QUERY, {'reply-to': Queues.QUEUE_RISK_RECOVERY_QUERY_RESPONSE + '.' + USERNAME, 'content-length': false});
}

export function applySolutionToLog(solution){
    client.send(Queues.QUEUE_RISK_RECOVERY_INVOKE, {'reply-to': Queues.QUEUE_RISK_RECOVERY_INVOKE_RESPONSE + '.' + USERNAME, 'content-length': false}, JSON.stringify(solution));
}

export function closePositions(batch){
    client.send(Queues.QUEUE_RISK_PLATFORM_SUBMIT_ORDER_BATCH, {'content-length': false}, JSON.stringify(batch));
}