import {SET_ACCOUNTS, SET_ACTIONS, SET_EXECUTIONS, SET_POSITIONS, SET_RISK_ALERTS, SET_RISK_RECOVERY, SET_CONNECTION_STATUS} from './actions';

const initialState = {
    sub: null,
    positions: null,
    executions: null,
    connectionStatus: '',
    connected: false,
    user: null,
    logs: null,
    shouldLoadLogs: false
};

export default function riskReducer(state = initialState, action = {}){
    switch(action.type){
        case SET_ACCOUNTS:
            return {...state, sub: action.sub};
        case SET_ACTIONS:
            return {...state};
        case SET_EXECUTIONS:
            return {...state, executions: action.executions};
        case SET_POSITIONS:
            return {...state, positions: action.positions};
        case SET_RISK_ALERTS:
            return {...state, alerts: action.alerts};
        case SET_RISK_RECOVERY:
            return {...state, logs: action.recoveries, shouldLoadLogs: false};
        case SET_CONNECTION_STATUS:
            return {...state, connectionStatus: action.status, user: action.user, connected: action.isConnected};
        default: return state;
    }
}