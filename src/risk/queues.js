/*export const TOPIC_EQUITY_PERCENTAGE = '/topic/risk.account.equitypercentage';
export const TOPIC_RISK_ALERT = '/topic/risk.alert';

export const QUEUE_UPDATE_TRADING_PERMISSION = '/queue/risk.platform.updatetradingpermission';
export const QUEUE_CLEAR_TRADING_PERMISSION = '/queue/risk.platform.cleartradingpermission';
export const QUEUE_RISK_RECOVERY_QUERY = '/queue/risk.recovery.query';
export const QUEUE_RISK_RECOVERY_QUERY_RESPONSE = '/queue/risk.recovery.query.response';
export const QUEUE_GET_EXECUTIONS = '/queue/execution.get';
export const QUEUE_GET_EXECUTIONS_RESPONSE = '/queue/execution.get.response';
export const QUEUE_RISK_RECOVERY_INVOKE = '/queue/risk.recovery.invoke';
export const QUEUE_RISK_RECOVERY_INVOKE_RESPONSE = '/queue/risk.recovery.invoke.response';
export const QUEUE_RISK_PLATFORM_MESSAGE = '/queue/risk.platform.message';
export const QUEUE_RISK_PLATFORM_SUBMIT_ORDER_BATCH = '/queue/risk.platform.submitorderbatch';
export const QUEUE_GET_POSITIONS = '/queue/position.get';
export const QUEUE_GET_POSITIONS_RESPONSE = '/queue/position.responses';*/

 export const TOPIC_EQUITY_PERCENTAGE = 'jms.topic.risk.account.equitypercentage';
 export const TOPIC_RISK_ALERT = 'jms.topic.risk.alert';

 export const QUEUE_UPDATE_TRADING_PERMISSION = 'jms.queue.risk.platform.updatetradingpermission';
 export const QUEUE_CLEAR_TRADING_PERMISSION = 'jms.queue.risk.platform.cleartradingpermission';
 export const QUEUE_RISK_RECOVERY_QUERY = 'jms.queue.risk.recovery.query';
 export const QUEUE_RISK_RECOVERY_QUERY_RESPONSE = 'jms.queue.risk.recovery.query.response';
 export const QUEUE_GET_EXECUTIONS = 'jms.queue.execution.get';
 export const QUEUE_GET_EXECUTIONS_RESPONSE = 'jms.queue.execution.get.response';
 export const QUEUE_RISK_RECOVERY_INVOKE = 'jms.queue.risk.recovery.invoke';
 export const QUEUE_RISK_RECOVERY_INVOKE_RESPONSE = 'jms.queue.risk.recovery.invoke.response';
 export const QUEUE_RISK_PLATFORM_MESSAGE = 'jms.queue.risk.platform.message';
 export const QUEUE_RISK_PLATFORM_SUBMIT_ORDER_BATCH = 'jms.queue.risk.platform.submitorderbatch';
 export const QUEUE_GET_POSITIONS = 'jms.queue.position.get';
 export const QUEUE_GET_POSITIONS_RESPONSE = 'jms.queue.position.responses';


