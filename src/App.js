import React from 'react'
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route } from 'react-router-dom';
import RiskApplication from './risk/containers/RiskApplication';
import LoginPanel from './risk/containers/login/LogInPanel';

// Enable responsiveConfig app-wide. You can remove this if you don't plan to build a responsive UI.
Ext.require('Ext.plugin.Responsive');

/**
 * The main application view
 */
export default function App() {

    return (
        <Router>
            <RiskApplication />
        </Router>
    )
    
}
