import React from 'react';
import { create } from 'react-test-renderer';
import MessageToBrokerForm from '../src/risk/components/MessageToBrokerForm';
import '../src/risk/components/model/model';

describe('Message To Broker Form', () => {
    it('should render without crashing', () => {
        const result = create(<MessageToBrokerForm  />);
        expect(result).toMatchSnapshot();
    })
});