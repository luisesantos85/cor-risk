import React from 'react';
import { create } from 'react-test-renderer';
import ClosePositionsPanel from '../src/risk/components/ClosePositionsPanel';
import '../src/risk/components/model/model';

describe('Close Positions Panel', () => {
    it('should render without crashing', () => {
        const result = create(<ClosePositionsPanel  />);
        expect(result).toMatchSnapshot();
    })
});