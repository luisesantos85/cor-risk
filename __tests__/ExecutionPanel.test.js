import React from 'react';
import { create } from 'react-test-renderer';
import ExecutionPanel from '../src/risk/components/ExecutionPanel';
import '../src/risk/components/model/model';

describe('Executions Panel', () => {
    it('should render without crashing', () => {
        const result = create(<ExecutionPanel />);
        expect(result).toMatchSnapshot();
    })
});