import React from 'react';
import { create } from 'react-test-renderer';
import MonitorPanel from '../src/risk/components/MonitorPanel';
import '../src/risk/components/model/model';

describe('Monitor Panel', () => {
    it('should render without crashing', () => {
        const result = create(<MonitorPanel />);
        expect(result).toMatchSnapshot();
    })
});