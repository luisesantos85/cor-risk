import React from 'react';
import { create } from 'react-test-renderer';
import AccountDetailForm from '../src/risk/components/AccountDetailForm';
import '../src/risk/components/model/model';

describe('Account Detail Form', () => {
    it('should render without crashing', () => {
        const result = create(<AccountDetailForm />);
        expect(result).toMatchSnapshot();
    })
});