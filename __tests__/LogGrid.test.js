import React from 'react';
import { create } from 'react-test-renderer';
import LogGrid from '../src/risk/components/LogGrid';
import '../src/risk/components/model/model';

describe('Log Grid', () => {
    it('should render without crashing', () => {
        const result = create(<LogGrid getRecoveries={() => {}} />);
        expect(result).toMatchSnapshot();
    })
});