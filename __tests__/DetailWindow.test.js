import React from 'react';
import { create } from 'react-test-renderer';
import DetailWindow from '../src/risk/components/DetailWindow';
import '../src/risk/components/model/model';

describe('Detail Window', () => {
    it('should render without crashing', () => {
        const result = create(<DetailWindow />);
        expect(result).toMatchSnapshot();
    })
});