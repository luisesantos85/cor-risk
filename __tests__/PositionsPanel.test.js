import React from 'react';
import { create } from 'react-test-renderer';
import PositionsPanel from '../src/risk/components/PositionsPanel';
import '../src/risk/components/model/model';

describe('Positions Panel', () => {
    it('should render without crashing', () => {
        const result = create(<PositionsPanel />);
        expect(result).toMatchSnapshot();
    })
});